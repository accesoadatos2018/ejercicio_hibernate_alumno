/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Tomas
 */
public class Aula {

    private int cod;
    private String nomaula;

    public Aula(String nomAula) {
        this.nomaula = nomAula;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNomaula() {
        return nomaula;
    }

    public void setNomaula(String nomaula) {
        this.nomaula = nomaula;
    }

}
