/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author Tomas Tortosa
 */
public class Grup implements Serializable {

    private String codi;
    private Nivell nivell;
    private Set<Alumno> llistaAlumnes;
    private Alumno delegat;
    private Aula aula;

    public Grup() {
    }

    public Grup(String codi, Nivell nivell, Aula a) {
        this.codi = codi;
        this.nivell = nivell;
        this.aula = a;

    }

    @Override
    public String toString() {
        return "Grup{" + "codi=" + codi + ", nivell=" + nivell + ", delegat=" + delegat + '}';
    }

    public void imprimirAlumnes() {
        for (Alumno a : llistaAlumnes) {
            System.out.println(a);
        }
    }

    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public Nivell getNivell() {
        return nivell;
    }

    public void setNivell(Nivell nivell) {
        this.nivell = nivell;
    }

    public Alumno getDelegat() {
        return delegat;
    }

    public void setDelegat(Alumno delegat) {
        this.delegat = delegat;
    }

    public Set<Alumno> getLlistaAlumnes() {
        return llistaAlumnes;
    }

    public void setLlistaAlumnes(Set<Alumno> llistaAlumnes) {
        this.llistaAlumnes = llistaAlumnes;
    }

    public Aula getA() {
        return aula;
    }

    public void setA(Aula a) {
        this.aula = a;
    }

}
