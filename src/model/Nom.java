/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Tomas Tortosa
 */
public class Nom implements Serializable{
    private String nom;

    public Nom() {
    }

    public Nom(String nom) {
        this.nom = nom;
    }

    public Nom(Nom nom, int i, Sexe sexe, Date time, int i0, Grup g) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @Override
    public String toString() {
        return "***** NOM ***** " + "\n" + this.getNom();
    }
    
}
