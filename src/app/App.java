/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import model.Alumno;
import model.Aula;
import model.Grup;
import model.Nivell;
import model.Nom;
import model.Sexe;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int opcion;
        Alumno a = null;
        Grup grup;
        Calendar dataNaix = Calendar.getInstance();
        Set<Alumno> llistaAlumnes = new HashSet<>();
        Grup g = null;
        Nivell n = null;
        Sexe s = null;
        Nom nom;

        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //CREAR UNA SESION
        Session session = factory.openSession();

        do {
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {

                case 0:// Salir
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo...");

                case 1: //Crear Grup
                    dataNaix.set(1994, 1, 5);
                    nom = new Nom("Anna");
                    Aula aula = new Aula("Informatica");
                    g = new Grup("G1J", n.Batxiller, aula);
                    a = new Alumno(nom, 962259898, s.Dona, dataNaix.getTime(), 0, g);
                    llistaAlumnes.add(a);
                    g.setLlistaAlumnes(llistaAlumnes);

                    g.setDelegat(a);

                    session.beginTransaction();
                    session.save(g);
                    session.getTransaction().commit();

                    break;

                case 2: //Llegir Grup
                    g = session.get(Grup.class, "B1A");
                    System.out.println(g);
                    g.imprimirAlumnes();
                    break;

                case 3: // Actualitzar Grup
                    session.beginTransaction();
                    g.setNivell(n.CF);
                    session.update(g);
                    session.getTransaction().commit();
                    break;

                case 4: //Eliminar Grup
                    session.beginTransaction();
                    session.delete(g);
                    session.getTransaction().commit();
                    break;

                case 5: // Crear Alumne
                    session.beginTransaction();

                    //OBTENIM EL GRUP I EL ARRAY ON VOLEM ANYADIR EL ALUMNE
                    g = session.get(Grup.class, "B1A");
                    llistaAlumnes = g.getLlistaAlumnes();

                    //CREEM EL ALUMNE
                    dataNaix.set(1990, 5, 22);
                    nom = new Nom("Sandra");
                    a = new Alumno(nom, 967876231, s.Dona, dataNaix.getTime(), 2, g);

                    //ANYADIM EL ALUMNE AL ARRAY I ACTUALITZEM EL GRUP
                    llistaAlumnes.add(a);
                    session.update(g);

                    session.getTransaction().commit();
                    break;

                case 6: // LLegir Alumne
                    a = session.get(Alumno.class, 1);
                    System.out.println(a);
                    break;

                case 7: // Actualitzar Alumne
                    session.beginTransaction();
                    nom = new Nom("Raul");
                    a.setNom(nom);
                    session.getTransaction().commit();
                    break;

                case 8: // Eliminar Alumne
                    session.beginTransaction();
                    g = session.get(Grup.class, "B1A");
                    llistaAlumnes = g.getLlistaAlumnes();
                    llistaAlumnes.remove(a);
                    g.setLlistaAlumnes(llistaAlumnes);
                    session.update(g);
                    session.getTransaction().commit();
                    break;

                case 9: // Realizar Consultas

                    // Nombre de los Alumnos mayores de 18 años
                    Query query = session.createQuery("SELECT a.nom FROM Alumno a WHERE sexe=0");
                    List<Nom> llistaNoms = query.list();

                    System.out.println("Nombre de los alumnos mayores de 18 años");

                    for (Nom nomb : llistaNoms) {

                        System.out.println(nomb);

                    }

                    //*******************************************************
                    // Nombre de los alumnos que has suspendido las mimas asignaturas que aquellos cuyo apellido empieza por F
                    query = session.createQuery("SELECT a.nom FROM Alumno a WHERE a.susp = (SELECT a.susp FROM Alumno a WHERE a.nom LIKE 'T%')");
                    llistaNoms = query.list();

                    System.out.println("Nombre Alumnos que han suspendido mis asignaturas que nombre empieza por T");

                    for (Nom nomb : llistaNoms) {

                        System.out.println(nomb);

                    }

                    //*******************************************************
                    // Nexp, nombre alumno y nombre grupo en que son delegados
                    query = session.createQuery("SELECT a.nexp , a.nom FROM Alumno a , Grup g WHERE a.grup = g.codi");

                    List<Object[]> llistaAlum = query.list();
                    for (Object[] o : llistaAlum) {
                        System.out.println(o[0] + " -- " + o[1]);
                    }

                    //*******************************************************
                    // Nombre Alumno que han suspendido más que la media
                    query = session.createQuery("SELECT a.nom FROM Alumno a WHERE susp > (SELECT AVG(a.susp) FROM Alumno a)");

                    llistaNoms = query.list();

                    System.out.println("Nombre Alumnos que han suspendido más que la media");

                    for (Nom nomb : llistaNoms) {

                        System.out.println(nomb);

                    }

                    // *******************************************************
                    // Nombre Alumnos que han suspendido más de 2 asignaturas
                    query = session.createQuery("SELECT a.nom FROM Alumno a WHERE susp > 2");

                    llistaNoms = query.list();

                    System.out.println("Nombre Alumnos que han suspendido más de 2 asignaturas");

                    for (Nom nomb : llistaNoms) {

                        System.out.println(nomb);

                    }

                    // *******************************************************
                    // Seleccionar Grupo y numero de alumnos matriculados en cada uno
                    query = session.createQuery("SELECT COUNT(*), g.codi FROM Grup g, Alumno a WHERE g.codi=a.grup GROUP BY g.codi");

                    llistaAlum = query.list();

                    for (Object[] o : llistaAlum) {
                        System.out.println(o[0] + " -- " + o[1]);
                    }

                    // *******************************************************
                    break;

                default:
                    throw new AssertionError();
            }
        } while (opcion != 10);

    }

    public static void menu() {

        System.out.println("0. Salir");
        System.out.println("1. Crear grup");
        System.out.println("2. Llegir grup");
        System.out.println("3. Actualitzar grup");
        System.out.println("4. Eliminar grup");
        System.out.println("5. Crear i guardar alumne");
        System.out.println("6. Llegir alumne");
        System.out.println("7. Actualitzar alumne");
        System.out.println("8. Eliminar alumne");
        System.out.println("9. Realizar Consultas");
    }
}
